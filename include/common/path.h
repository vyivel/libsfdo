#ifndef PATH_H
#define PATH_H

#include <stdbool.h>
#include <stddef.h>

static inline bool sfdo_path_needs_extra_slash(const char *path, size_t len) {
	return len >= 2 && path[len - 1] != '/';
}

static inline size_t sfdo_path_compute_mem_size(const char *path, size_t len) {
	// 1 for NUL + potential 1 for slash
	return len + (sfdo_path_needs_extra_slash(path, len) ? 2 : 1);
}

#endif
