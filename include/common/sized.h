#ifndef SIZED_H
#define SIZED_H

#include <stddef.h>

#define SFDO_SIZED_LEN(s) (sizeof(s) - 1)

#define SFDO_SIZED_LIT(s) {s, (sizeof(s) - 1)}

#define SFDO_SIZED_CHAR(c) {(char[]){c}, 1}
#define SFDO_SIZED_NUL SFDO_SIZED_CHAR('\0')
#define SFDO_SIZED_SLASH SFDO_SIZED_CHAR('/')

#endif
