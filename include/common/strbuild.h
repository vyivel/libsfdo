#ifndef STRBUILD_H
#define STRBUILD_H

#include <sfdo-common.h>
#include <stdbool.h>
#include <stddef.h>

struct sfdo_strbuild {
	char *data;
	size_t len, cap;
};

void sfdo_strbuild_init(struct sfdo_strbuild *strbuild);
void sfdo_strbuild_finish(struct sfdo_strbuild *strbuild);

void sfdo_strbuild_reset(struct sfdo_strbuild *strbuild);

bool sfdo_strbuild_add(
		struct sfdo_strbuild *strbuild, const struct sfdo_string *items, size_t n_items);

#define SFDO_STRBUILD_ADD(strbuild, ...) \
	sfdo_strbuild_add(strbuild, (struct sfdo_string[]){__VA_ARGS__}, \
			sizeof((struct sfdo_string[]){__VA_ARGS__}) / sizeof(struct sfdo_string))

#endif
