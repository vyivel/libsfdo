#ifndef MEMBUILD_H
#define MEMBUILD_H

#include <assert.h>
#include <sfdo-common.h>
#include <stdbool.h>
#include <stddef.h>

struct sfdo_membuild {
	char *data;
	size_t len;
	size_t cap;
	size_t last_nulterm_len;
};

bool sfdo_membuild_setup(struct sfdo_membuild *membuild, size_t cap);

static inline char *sfdo_membuild_curr(struct sfdo_membuild *membuild) {
	return &membuild->data[membuild->len];
}

void sfdo_membuild_add(
		struct sfdo_membuild *membuild, const struct sfdo_string *items, size_t n_items);

void sfdo_membuild_add_path_segment(struct sfdo_membuild *membuild, const char *data, size_t len);

size_t sfdo_membuild_nulterm(struct sfdo_membuild *membuild);

static inline void sfdo_membuild_validate(struct sfdo_membuild *membuild) {
	assert(membuild->len == membuild->cap);
}

#define SFDO_MEMBUILD_ADD(membuild, ...) \
	sfdo_membuild_add(membuild, (struct sfdo_string[]){__VA_ARGS__}, \
			sizeof((struct sfdo_string[]){__VA_ARGS__}) / sizeof(struct sfdo_string))

#endif
