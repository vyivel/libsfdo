include:
  - project: "freedesktop/ci-templates"
    ref: b61a03cabbf308e81289f7aaaf0b5a80a34ffb99
    file: "/templates/fedora.yml"

variables:
  FDO_UPSTREAM_REPO: vyivel/libsfdo
  MESON_COMMON_OPTIONS: >
    --fatal-meson-warnings
    -Dwerror=true
    -Db_sanitize=address,undefined

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      # Don't duplicate pipelines
      when: never
    - if: $CI_COMMIT_BRANCH

stages:
  - prep
  - style-check
  - build

.policy:
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure
  interruptible: true

.fedora-41:
  extends:
    - .policy
  variables:
    FDO_DISTRIBUTION_VERSION: "41"
    FDO_DISTRIBUTION_TAG: "2024-12-25.1"

prep-fedora-41:
  extends:
    - .fdo.container-build@fedora
    - .fedora-41
  stage: prep
  variables:
    GIT_STRATEGY: none
    FDO_DISTRIBUTION_PACKAGES: >
      clang
      clang-analyzer
      clang-tools-extra
      compiler-rt
      gcc
      git-core
      lcov
      libasan
      libasan-static
      libubsan
      libubsan-static
      lld
      meson
      pkgconf-pkg-config

.image-fedora:
  extends:
    - .fdo.distribution-image@fedora
    - .fedora-41

clang-format:
  extends:
    - .image-fedora
  stage: style-check
  script:
    - meson setup build/
    - ninja -C build/ clang-format-check

.build:
  extends:
    - .image-fedora
  stage: build
  artifacts:
    when: always
    expire_in: 1 week
    paths:
      - "build/meson-logs"

.build-and-test:
  extends:
    - .build
  script:
    - >
      meson setup build/
      ${MESON_COMMON_OPTIONS}
      ${MESON_OPTIONS}
    - ninja -C build/ -k0 -j${FDO_CI_CONCURRENT:-4}
    - meson test -C build/ --num-processes ${FDO_CI_CONCURRENT:-4}

build-clang:
  extends:
    - .build-and-test
  variables:
    CC: clang
    CC_LD: lld
    MESON_OPTIONS: >
      -Db_lundef=false

build-release:
  extends:
    - .build-and-test
  variables:
    MESON_OPTIONS: >
      -Dbuildtype=release

build-scan:
  extends:
    - .build
  script:
    - >
      meson setup build/
      ${MESON_COMMON_OPTIONS}
      ${MESON_OPTIONS}
    - ninja -C build/ scan-build -k0 -j${FDO_CI_CONCURRENT:-4}

build-coverage:
  extends:
    - .build
  script:
    - meson setup build/ ${MESON_COMMON_OPTIONS} -Db_coverage=true
    - ninja -C build/ -k0 -j${FDO_CI_CONCURRENT:-4}
    - mkdir -p build-coverage/
    - >
      lcov
      --config-file .lcovrc
      --directory build/
      --capture
      --initial
      --output-file "build-coverage/baseline.lcov"
    - meson test -C build/ --num-processes ${FDO_CI_CONCURRENT:-4}
    - >
      lcov
      --config-file .lcovrc
      --directory build/
      --capture
      --output-file "build-coverage/test.lcov"
    - >
      lcov
      --add-tracefile "build-coverage/baseline.lcov"
      --add-tracefile "build-coverage/test.lcov"
      --output-file "build-coverage/out.lcov"
  artifacts:
    paths:
      - "build-coverage/"
  coverage: '/^\s+lines\.+:\s+([\d.]+\%)\s+/'
